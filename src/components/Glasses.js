import React, { Component } from "react";
import dataGlasses from "../data/dataGlasses.json";
class Glasses extends Component {
  state = {
    glassContent: dataGlasses[0],
  };
  handleRenderGlasses = () => {
    return dataGlasses.map((item, index) => {
      return (
        <div
          className="glasess__item col-3 mt-5 ml-5"
          key={index}
          onClick={() => this.handleChangeGlasses(item)}
        >
          <img src={item.url} alt="" />
        </div>
      );
    });
  };
  handleChangeGlasses = (glasess) => {
    this.setState({
      glassContent: glasess,
    });
  };
  render() {
    return (
      <div className="container container__template">
        <div>
          <div className="container__glasses">
            <div className="avatar my-5">
              <img src={this.state.glassContent.url} alt="hinh anh" />
              <div className="glasses__content pl-3">
                <h4>{this.state.glassContent.name}</h4>
                <h6 className="text text-warning">
                  ${this.state.glassContent.price}
                </h6>
                <p>{this.state.glassContent.desc}</p>
              </div>
            </div>
          </div>
        </div>
        <div className="row">{this.handleRenderGlasses()}</div>
      </div>
    );
  }
}
export default Glasses;
